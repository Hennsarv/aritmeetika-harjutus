﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TryCathcDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\andmed.txt";
            try
            {
                Console.WriteLine(
                File.ReadAllLines(filename)
                    .Select(x => int.Parse(x.Trim()))
                    .Average()
                );
            }
            //catch (IOException e)
            //{
            //    Console.WriteLine($"kas faili ei ole? {filename}");
            //}
            catch (IOException ioe)
            {
                Console.WriteLine("faili ei ole");
            }
            catch (Exception e)
            {
                //switch(e)
                //{
                //    case IOException ioe:
                //        Console.WriteLine("faili pole");
                //        break;
                //    case FormatException fe:

                //        break;
                //    default:
                //        Console.WriteLine($"midagi juhtus: {e.Message} {e.GetType().Name}");
                //        break;
                //}

                Console.WriteLine($"midagi juhtus: {e.Message} {e.GetType().Name}");
            }
            

        }
    }
}
