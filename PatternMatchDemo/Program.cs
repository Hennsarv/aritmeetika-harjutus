﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PatternMatchDemo
{
    static class Program
    {
        static void ForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (T x in m) a(x);
        }

        static IEnumerable<T> ForEach2<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach(T x in m) { a(x); yield return x; }
        }


        static double kordaja = 2;
        static double Miski(double x)
        {
            // teeb ka veel midagi
            return x * kordaja;
        }

        static void Main(string[] args)
        {
            Func<double, double> tee = x => { Console.WriteLine(x); return x; };
            List<Kujund> kujundid = new List<Kujund>
            {
                new Ring(10),
                new Ring(2),
                new Ruut(7),
                new Lill {Varrepikkus = 7, Õielehtedearv = 18 },
                new Ristkülik(10,20)
            };

            

            Console.WriteLine(kujundid.Select(x => x.Pindala).Select(x => { Console.WriteLine(x); return x; }).Sum());

            int lisa = 4;
            double koguja = 0;

            double[] arvud = { 1, 2, 3, 4, 5 };
            Console.WriteLine(arvud.Select(x => { koguja += x; return x * kordaja + lisa ; }).Sum());

            Console.WriteLine(koguja);

            Console.WriteLine("\nimeliknäide\n");





        }
    }

    class Kujund
    {
        public double Pindala
        {
            get
            {
                switch(this)
                {
                    case Ring ring: return Math.PI * ring.Raadius * ring.Raadius;
                    case Ruut ruut: return ruut.Külg * ruut.Külg;
                    case Ristkülik külik when külik.Alus != 0: return külik.Alus * külik.Kõrgus;
                    default: return 0;
                }
            }
        }
        public double Ümbermõõt
            => this switch
            {
                Ring ring => ring.Raadius * 2 * Math.PI,
                Ruut ruut => ruut.Külg * 4,
                Ristkülik ristkülik => (ristkülik.Alus + ristkülik.Kõrgus) * 2,
                Lill lill => lill.Varrepikkus * 2 + lill.Õielehtedearv * 4,
                _ => 0
            };
    }
    class Ring : Kujund
    {
        public double Raadius;
        public Ring(double raadius) => Raadius = raadius;
    }
    class Ristkülik: Kujund
    {
        public double Alus;
        public double Kõrgus;
        public Ristkülik(double alus, double kõrgus) => (Alus, Kõrgus) = (alus, kõrgus);
    }

    class Ruut : Kujund
    {
        public double Külg;
        public Ruut(double külg) => Külg = külg;
    }

    class Lill : Kujund
    {
        public double Varrepikkus;
        public int Õielehtedearv;
    }
}
