﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom lkroko = new Loom { Liik = "krokodill" };
            Loom lnahk = new Loom { Liik = "nahkhiir" };
            Loom laisik = new Loom("laiskloom") { };
            Loom hirmus = new Loom();

            Loom sipelg = ("sipelgas", 6);

            Loom kaisukas = ("karu", 4);

            Koer pontu = new Koer("Pontu" );
            Kass k = new Kass("Miisu");

            k.Silita();

            var imelik = sipelg + kaisukas;

            Console.WriteLine(string.Join(values: Loom.Loomaaed, separator: "\n"));

            Console.WriteLine("\nMürgel hakkab:\n");

            foreach (var l in Loom.Loomaaed) l.TeeHäält();

            k.Sikut();
            k.TeeHäält();


            Sepik s = new Sepik();
            Lõuna(k);
            

        }

        static void Lõuna(object o)
        {
            // if (o is ISöödav s) s.Süüakse(); else Console.WriteLine("nälg majas");

            Eatable s = o as Eatable; // o oli söödav, siis saame me süüa
                                      // muidu s == null

            (o as Eatable)?.Süüakse();

            Eatable ss = (o is Eatable) ? (Eatable)o : null;




        }

    }
    class Loom
    {
        static List<Loom> _Loomaaed = new List<Loom>();
        public static IEnumerable<Loom> Loomaaed => _Loomaaed.AsEnumerable();
        public string Liik { get; set; }
        public int Jalgu { get; }

        public Loom() =>_Loomaaed.Add(this);
        // konstruktor - default moodi
        
        public Loom(string liik, int jalgu = 0) : this() => (this.Liik, this.Jalgu) = (liik, jalgu);
        // parameetriga konstruktor

        public override string ToString() => $"Loom liigist {Liik} tal on {Jalgu} jalga";

        public static implicit operator Loom((string liik, int jalgu) sisu) => new Loom(sisu.liik, sisu.jalgu);

        public static Loom operator +(Loom y, Loom t) => new Loom(y.Liik + "-" + t.Liik, Math.Max(y.Jalgu, t.Jalgu));

        public virtual void TeeHäält() => Console.WriteLine($"{this} teeb koledat häält");
        // virtual tähendab, et tuletatud klass võib selle omal moel kirjutada
    }

    abstract class Koduloom : Loom   // tuletatud klass
    {
        public string Nimi { get; set; }

        public Koduloom(string liik, string nimi) : base(liik, 4)
        {
            Nimi = nimi;
        }

        public override string ToString() => $"{Liik} {Nimi}";

        public abstract string Omanik { get; set; }

    }

    class Kass : Koduloom
    {
        public string Tõug { get; set; }
        bool Tuju = false;
        private string kes;

        public Kass(string nimi) : base("kass", nimi) { }

        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kiisu {Nimi} lööb nurru");
            else Console.WriteLine($"Kass {Nimi} kräunub koledasti");
        }

        public void Silita() => Tuju = true;
        public void Sikut() => Tuju = false;

        public override string Omanik 
        { 
            get => kes; 
            set => kes = value ; 
        }
    }

    class Koer : Koduloom, Eatable
    {
        int omanikukood;

        public Koer(string nimi) : base("koer", nimi) { }

        public override string Omanik 
        { 
            get => $"omanik: {omanikukood}"; 
            set => omanikukood = int.Parse(value.Replace("omanik: ", "")); 
        }

        public void Süüakse()
        {
            Console.WriteLine($"Koer {Nimi} pistetakse nahka") ;
        }

        public override void TeeHäält() => Console.WriteLine($"{Nimi} haugub");
    }

    public class Sepik : Eatable
    {
        public void Süüakse()
        {
            Console.WriteLine("Keegi nosib sepikut"); ;
        }
    }

    interface Eatable
    {
        void Süüakse();
    }

}
