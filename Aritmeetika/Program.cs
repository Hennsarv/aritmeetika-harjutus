﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aritmeetika 
{
    // alustame versioonidega
    class Program
    {
        static void Main(string[] args)
        {

            Liida(1, 2, 3, 4, 5, 6, 7);


            // hakatuseks teen omale randomi ja panen hindeks maksimumpunktid
            Random r = new Random();
            int hinne = 60;

            var nimed = LoeKaksNime("anna kaks nime");
            Console.WriteLine($"eesnimi: {nimed.eesnimi} perenimi: {nimed.perenimi}");

            string eesnimi;
            string perenimi;
            AnnaKaksNime("anna kaks nime", out eesnimi, out perenimi);
            Console.WriteLine($"eesnimi: {eesnimi} perenimi: {perenimi}");

            bool bb = TryKaksNime("veelkord kaks nime", out string firstname, out string lastname);
            Console.WriteLine($"eesnimi: {firstname} perenimi: {lastname}");
            if (!bb) Console.WriteLine("sa andsid ainult ühe nime");




            // siis teen kümme ülesannet (tsükkel)
            for (int i = 0; i < 1; i++)
            {
                string küsimus;
                string märk="";
                double vastus = 0;
                double a = r.Next(100) + 1;
                double b = r.Next(1, 101);
                int tehe = r.Next(4); // 0, 1, 2 või 3

                //switch (tehe)   // kirjutatud switch-blokina
                //{
                //    case 0: // liitmine
                //        //(märk, vastus) = ("+", a + b);
                //        märk = "+";
                //        vastus = a + b;
                //        break;
                //    case 1: // lahutamine
                //        (märk, vastus) = ("-", a - b);
                //        break;
                //    case 2: // korrutamine
                //        (märk, vastus) = ("*", a * b);
                //        break;
                //    case 3: // jagamine
                //        (märk, vastus) = ("/", a / b);
                //        break;
                //        } 


                (märk, vastus) = tehe switch // switch avaldis
                {
                    0 => ("+", a + b),
                    1 => ("-", a - b),
                    2 => ("*", a * b),
                    4 => ("/", a / b),
                    _ => ("", 0)        // switchi mõttes default
                };
                küsimus = $"palju on {a}{märk}{b}: ";
                for (int j = 0; j < 6; j++)
                {
                    Console.Write($"{i+1}. ülesanne ({j+1}. katse) {küsimus}");
                    if (double.TryParse(Console.ReadLine(), out double katse) && Math.Abs(katse-vastus) < 0.05) break;
                    Console.WriteLine("läks pisut mööda");
                    hinne--;
                }
            }
            // lõpuks annan tulemuseks hinde

            // variant if elsiffidega
            //string tulemus;
            //if (hinne == 60) tulemus = "hiilgav";
            //else if (hinne > 40) tulemus = "väga hea";
            //else if (hinne > 25) tulemus = "tubli";
            //else if (hinne > 12) tulemus = "hakkab tulema";
            //else if (hinne > 5) tulemus = "juba läheb";
            //else tulemus = "mine õppima";

            //variant ?: tehtega
            string tulemus =
                hinne == 60 ? "hiilgav" :
                hinne > 40 ? "väga hea" :
                hinne > 25 ? "tubli" :
                hinne > 12 ? "hakkab tulema" :
                hinne > 5 ? "juba läheb" :
                "mine õppima";

            Console.WriteLine($"sinu tulemus oli {hinne} - {tulemus}");

            Console.ReadKey();
        }

        public static (string eesnimi,string perenimi) LoeKaksNime(string kysimus)
        {
            Console.Write(kysimus+": ");
            string[] loetud = (Console.ReadLine()+" ").Split(' ');
            return (loetud[0], loetud[1]);
        }

        public static void AnnaKaksNime(string küsimus, out string eesnimi, out string perenimi)
        {
            Console.Write(küsimus + ": ");
            string[] loetud = (Console.ReadLine() + " ").Split(' ');
            eesnimi = loetud[0];
            perenimi = loetud[1];
        }

        public static bool TryKaksNime(string küsimus, out string eesnimi, out string perenimi)
        {
            perenimi = "";
            Console.Write(küsimus + ": ");
            string[] loetud = (Console.ReadLine()).Split(' ');
            eesnimi = loetud[0];
            if (loetud.Length < 2) return false;
            
            return true;
        }


        static void Demo(int a = 0, int b = 0)
        { }


        public static long Liida(int a, int b) => a + b + 100;
        public static long Liida(int a, int b, int c) => a + b + c + 100;

        static int Liida(params int[] muud)
        {
            int a = 0;
            foreach (var x in muud) a += x;
            return a;
        }
    }
}
