﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatukeMassiividest
{
    class Program
    {
        static void Main(string[] args)
        {
            // massiiv massiividest
            int[][] imelikAsi =
            {
                new int[] {1,2,3,4},
                new int[] {5,6},
                new int[] {7,8,9},
            };
            foreach (var x in imelikAsi)
            {
                foreach (var y in x) Console.Write($"{y} ");
                Console.WriteLine();
            }

            // list massiividest
            List<int[]> arvudlist = new List<int[]>();
            arvudlist.Add(new int[] { 1, 2, 3 });

            // massiiv listidest
            List<int>[] listArvud = new List<int>[3];
            listArvud[0] = new List<int> { 1, 2, 3 };

            List<List<int>> listListid = new List<List<int>>();

            // c# on aga Javas ei ole - 2-mõõtmeline massiv

            int[,] Tabel =
            {
                {1,2,3,4 },
                {5,6,7,8 }
            };

            Console.WriteLine(Tabel[1,2]);

            int[] lihtne = { 1, 2, 3, 4, 5 };
            var misseeon = lihtne.ToList();
            misseeon.Add(7);
            lihtne = misseeon.ToArray();

            



        }
    }
}
