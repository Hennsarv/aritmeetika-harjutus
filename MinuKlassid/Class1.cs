﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MinuKlassid
{
    public class Person
    {
        public static int Count { get; private set; }
        static List<Person> _People = new List<Person>();
        static public IEnumerable<Person> People => _People.AsEnumerable();
        // IEnumerable tagab, et selle asjaga saab teha AINULT foreach


        string _FirstName; string _LastName;  // need väljad tegin ma, et saaks nime teisenduse lisada propertyle
        public string PersonID { get; } // see tuleb veel read once (immutable) väljaks teha
        public string FirstName { get => _FirstName; set => _FirstName = F.ToTitle(value); }  // allpool kuskil ka funktsioon
        public string LastName { get => _LastName; set => _LastName = F.ToTitle(value); }
        public string FullName => $"{FirstName} {LastName}"; // lihtne r/o property
        public Gender Gender => (Gender)(PersonID[0] % 2);   // siin vaja pisut mõtlemist, et aru saada

        public int CompareTo(Person teine) => this.FullName.CompareTo(teine.FullName);
        public static int Compare(Person yks, Person teine) => yks.FullName.CompareTo(teine.FullName);


        public DateTime BirtDate => new DateTime(
            (18 + (PersonID[0] - '1') / 2) * 100 + int.Parse(PersonID.Substring(1, 2)),
            int.Parse(PersonID.Substring(3, 2)),
            int.Parse(PersonID.Substring(5, 2))
            );

        // see siin on konstruktor
        public Person(string personID)   // new Person()
        {
            PersonID = personID;
            Count++;
            _People.Add(this);     // this tähendab seda eksmeplari, mis sjust tehti
        }




        public int Age => (DateTime.Today - BirtDate).Days * 4 / 1461;

        public override string ToString() => $"{Age} year old {Gender} {FullName}";

        //public override string ToString()
        //{
        //    return $"inimene {FullName} kes on sündinud {BirtDate}" ;
        //}


    }

    public enum Gender
    { female, male, naine = 0, mees = 0 }

    public class F
    {
        public static string ToTitle(string n)
        //=> n == "" ? "" :
        //    string.Join(" ", n.Split(' ').Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower()));

        {
            if (n == "") return n;
            var nn = n.Split(' ');
            for (int i = 0; i < nn.Length; i++)
            {
                nn[i] = nn[i].Substring(0, 1).Trim() + nn[i].Substring(1).ToLower();
                
            }
            return string.Join(" ", nn);
        }

    }

}
