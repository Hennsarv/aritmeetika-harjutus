﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segamine
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> pakk = Enumerable.Range(0, 52).ToList();
            int[] pakk2 = new int[52];
            for (int i = 0; i < 52; i++)
            {
                pakk2[i] = i;
            }
            // miks mulle meeldib list - see selgub kohe
            var pakk3 = pakk2.ToList();
            // või
            var pakk4 = new List<int>();
            for (int i = 0; i < 52; i++)
            {
                pakk4.Add(i);
            }
            // kõigil juhtudel saan listi (või massiivi) milles 0..51

            int n = 0;
            foreach(var x in pakk)
            Console.Write($"{(Mast)(x / 13)} {(Kaart)(x % 13)} {(++n% 4 == 0 ? "\n" : "\t")}");
//            Console.Write($"{x:00} {(++n % 4 == 0 ? "\n" : "\t")}");

            // ma nüüd hakkan segama
            // selleks on mitu võimalust

            Random r = new Random();

            for (int i = 0; i < 10000; i++)
            {
                int n1 = r.Next(52);
                int n2 = r.Next(52);
                (pakk2[n1], pakk2[n2]) = (pakk2[n2], pakk2[n1]);
            }

            Console.WriteLine("\nnüüd segatud vahetamistega\n");
            n = 0;
            foreach (var x in pakk2)
                //                Console.Write($"{(Mast)(x / 13)} {(Kaart)(x % 13)} {(++i % 4 == 0 ? "\n" : "\t")}");
                Console.Write($"{x:00} {(++n % 4 == 0 ? "\n" : "\t")}");

            HashSet<int> pakk5 = new HashSet<int>(1003);
            while(pakk5.Count < 52)
            {
                pakk5.Add(r.Next(52));
            }

            Console.WriteLine("\nnüüd segatud hash-setiga\n");

            n = 0;
            foreach (var x in pakk5)
                //                Console.write($"{(mast)(x / 13)} {(kaart)(x % 13)} {(++i % 4 == 0 ? "\n" : "\t")}");
                Console.Write($"{x:00} {(++n % 4 == 0 ? "\n" : "\t")}");

            // üks segamise variant
            List<int> pakk6 = new List<int>();
            while(pakk.Count > 0)
            {
                int ajuti = r.Next(pakk.Count);
                pakk6.Add(pakk[ajuti]);
                pakk.RemoveAt(ajuti);
            }

            Console.WriteLine("\nnüüd segatud kohjast teise tõstmisega\n");
            n = 0;
            foreach (var x in pakk6)
                //                Console.write($"{(mast)(x / 13)} {(kaart)(x % 13)} {(++i % 4 == 0 ? "\n" : "\t")}");
                Console.Write($"{x:00} {(++n % 4 == 0 ? "\n" : "\t")}");

            

        }
    }

    enum Mast { Risti, Ruutu, Ärtu, Poti}
    enum Kaart { 
        Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme,
        Soldat, Emand, Kuningas, Äss    
    }
}
