﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KoduneYlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\raffas.txt";
            foreach(var rida in File.ReadAllLines(filename))
            {
                var nimed = rida.Split(' ');
                //Person p = Person.Create(nimed[0]);
                //if(p != null)
                //{
                //    p.Nimi =nimed[1];
                //}
                //else Console.WriteLine($"vigane rida (duplikaat) {rida}");

                if (Person.TryCreate(nimed[0], out Person p)) { p.Nimi = nimed[1]; }
                else Console.WriteLine($"vigane rida (duplikaat) {rida}");

            }

            foreach(var p in Person.People.Values)
            {
                Console.WriteLine($"laps: {p.Nimi} isa: {p.Vanemad[1]?.Nimi??"pole"}");
            }

        }
    }

    class Person
    {

        public static Dictionary<string, Person> People = new Dictionary<string, Person>();

        public string IK { get; }
        public string Nimi { get; set; }
        public List<Person> Lapsed { get; } = new List<Person>();
        public Person[] Vanemad { get; } = new Person[2];

        private Person(string IK)  // private konstruktor
        {
            People.Add(IK,this); // annab vea, kui sellline IK on juba meil kirjas
            // People[IK] = this;   // see ei anna viga, aga varasem 'unustatakse'
            this.IK = IK;
        }

        public static Person Create(string IK) // public funktsioon loomiseks
        {
            //if (People.ContainsKey(IK)) return null;
            //else return new Person(IK);

            return People.ContainsKey(IK) ? null : new Person(IK);

        }

        public static bool TryCreate(string IK, out Person p)
        {
            return (p = People.ContainsKey(IK) ? null : new Person(IK)) != null;
        }

        // factory pattern - st uute objetide loomiseks on mul kontrollitud funktsiion
        // klassi sees

        public static void AssignParents(string IK, string emaIK, string isaIK)
        {
            Person laps = People.ContainsKey(IK) ? People[IK] : null;
            Person ema = People.ContainsKey(emaIK) ? People[emaIK] : null;
            Person isa = People.ContainsKey(isaIK) ? People[isaIK] : null;
            if (laps != null)
            {
                laps.Vanemad[0] = ema;
                laps.Vanemad[1] = isa;
                ema?.Lapsed.Add(laps);                  // lühem variant
                if (isa != null) isa.Lapsed.Add(laps);  // pikem variant
            
            }

        }


    }
}
