﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectiElutsükkel
{
    class Program
    {
        static void Main(string[] args)
        {
            
            while(true)
            {
                //using (
                Test t = new Test { Nimi = "Henn" };
                //)
                {

                    Console.WriteLine(t);
                }  // t.Dispose();
            }
        }
    }

    class A
    {
        public string Nimi;
        public int Sünniaasta;

        // private konstruktor
        A(string nimi, int sünniaasta) => (Nimi, Sünniaasta) = (nimi, sünniaasta);

        public int Vanus => DateTime.Now.Year - Sünniaasta;

        public static implicit operator A((string nimi, int sünniaasta) d) => new A(d.nimi, d.sünniaasta);
    }

    class B // teeme klassi, mda saab vaid 1 maailmas olla
    {
        public static B BB { get; private set; } // = new B();

        static B()
        {
            BB = new B();
        }

        B() { }
    }

    //class C : B
    //{

    //    protected C() : base() { }
    //}

    class Test : IDisposable    
    {
        static List<Test> testid = new List<Test>();
        static int nr = 0;
        public int Nr { get; } = ++nr;
        public string Nimi;
        private bool disposedValue;

        public override string ToString() => $"{Nr}. {Nimi}";

        static Test() // static construktor
        {

        }


        public Test()
        {
            testid.Add(this);
            Console.WriteLine($"Tehti object {this}"); }

        public void Puhasta()
        {
            GC.SuppressFinalize(this);
        }

        ~Test()  // destructor
        {
            Console.WriteLine($"mälust lahkub {this}");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Test()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    class Yxik
    {
        static Yxik ainumas = null;

        static string _Nimi;
        static int _Vanus;
        
        public string Nimi { get => _Nimi; set => _Nimi = value; }
        public int Vanus { get => _Vanus; set => _Vanus = value; }

        private Yxik() { }

        public static Yxik Get() =>
        
            ainumas == null ? ainumas = new Yxik() : ainumas;

        // ettevaatust - parem kui sellist asja ei tee
        public static void Delete() => ainumas = null;
        
    }



}
