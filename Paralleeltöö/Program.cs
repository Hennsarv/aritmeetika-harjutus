﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace Paralleeltöö
{
    class Program
    {
        static long  test = 0;
        static List<long> tulemus = new List<long>();

        static long nr = 0;
        static Random r = new Random();
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(Tegevus));

            t1.Start();
            Thread t2 = new Thread(new ThreadStart(Tegevus));
            t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine(tulemus.Count);
            Console.WriteLine(string.Join(" ", tulemus));

        }

        static void Tegevus()
        {
            Random rr = new Random(r.Next());
            //int tegevus = ++nr;
            for(int i = 0; i < 100000 ;i++)
            {
                //Console.WriteLine($"Tegevus {tegevus}  raporteerib {i+1}. korda");
                //Thread.Sleep(10 * rr.Next(100));

                {
                    tulemus.Add(test++);
                }
            }
        }

    }
}
