﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIsOnRekursioon
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Faktoriaal2(70));
        }

        static double Faktoriaal(double x) => x < 2 ? 1 : Faktoriaal(x - 1) * x;

        static double Faktoriaal2(double x)
        {
            double tulemus = 1;
            for (int i = 1; (double)i <= x; i++)
            {
                tulemus *= i;
            }
            return tulemus;
        }
        

    }
}
