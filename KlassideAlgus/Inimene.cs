﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideAlgus
{
    class Inimene
    {
        // klassi väljad
        public string EesNimi; // field          // sõnast public räägime hiljem
        public string PereNimi; 
        int _Vanus;



        // klassil võivad olla propertid

        public int Vanus  // read-write property
        {
            get => _Vanus;
            set { if (value < 100 && value > 0) this._Vanus = value; }
            //set => _Vanus = (value < 100 && value > 0) ? value : _Vanus;
        }

        public string Nimi // read only property - mis sisaldab vaid get meetodit
         => $"{EesNimi} {PereNimi}";  // koosneb vaid get-funktsioonist
        


        // klassi meetodeid ja funktsioone

        //public void SetVanus(int vanus)
        //{
        //    if (vanus < 100 && vanus > 0) this._Vanus = vanus;
        //}

        //public int GetVanus() { return _Vanus; }

        public override string ToString() => $"{EesNimi} {PereNimi} vanusega {_Vanus}"; 

        

    }

}
