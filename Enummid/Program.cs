﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enummid
{
    class Program
    {
        static void Main(string[] args)
        {
            Kangelane k = (Kangelane)7;

            k = k | Kangelane.Kolisev;
            k ^= Kangelane.Puust;    // 1111
                                     // 0001
                                     //^1110

            

            Console.Write("Kes sa oled:");
            k = (Kangelane)Enum.Parse(typeof(Kangelane),Console.ReadLine(),true);
            Console.WriteLine(k);
            Console.WriteLine((int)k);
            Console.Write("Mis nädalapäeval sa tuled: ");
            DayOfWeek dof = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Console.ReadLine(), true);

            Console.WriteLine(dof);

        }
    }

    // klass see on andmetüüp ja andmetüüp see on klass

    enum Mast
    {
        Risti, Ruutu, Ärtu, Poti, Pada=3
    }

    [Flags]
    enum Kangelane
    {
        Puust=1, Punane=2, Suur=4, Kolisev=8
    }


}
