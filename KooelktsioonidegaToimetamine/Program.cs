﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace KooelktsioonidegaToimetamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ma räägin natuke keerulist juttu ka");

            //Console.Write("Kes sa oled: ");
            //Console.WriteLine(
            //    Console.ReadLine()
            //    .ToTitle()
            //    .Tere()
            //    );
            int[] arvud = { 1, 3, 2, 4, 5, 9, 6, 8, 0 };
            Console.WriteLine(string.Join(",", arvud));
            Console.WriteLine(string.Join(",", arvud.Where(x => x%2 == 0)));

            var q1 = arvud.Where(x => x % 2 == 0)
                    .Select(x => x * x)
                    ;

            var q2 = (
                from x in arvud
                where x % 2 == 0
                select x * x
                ).JoinToString(",");




            Console.WriteLine(arvud.OrderBy(x => x).JoinToString(","));

            Console.WriteLine(
                arvud
                .Select(x => DateTime.Today.AddMonths(x).ToShortDateString())
                
                .JoinToString(",")
                );


            //foreach (var n in arvud.Paaris()) Console.WriteLine(n);
            //foreach (var n in E.Paaris(arvud)) Console.WriteLine(n);


            //foreach (var n in arvud.Milline(x => x > 4)) Console.WriteLine(n); 

            //arvud.Milline(x => x % 2 == 0).Mida(x => Console.WriteLine(x));

            //int summa = 0;
            //arvud.Mida(x => summa += x);
            //Console.WriteLine(summa);

            arvud.Where(x => x % 2 == 0).Mida(x => Console.WriteLine(x));


            #region väljakommenteeritud
            //Mingi mingi = new Mingi { A = 7 };

            //mingi.Meetod();
            //E.Uus(mingi);
            //mingi.Ruut().Uus();


            //string s = " tore lugu   "
            //    .Trim()
            //    .Split(' ')[0]
            //    .ToUpper()
            //    .Substring(3, 3)
            //    .Replace("t", "õ")
            //    ;

            /*
             * replace(substring(toupper(split(' ',trim(s))[0]),3,3),"t","õ")
             */
            #endregion
        }


    }

    class Mingi
    {
        public int A;

        public void Meetod()
        {
            Console.WriteLine(this.A);
        }

        public static void Meetod(Mingi m)
        {
            Console.WriteLine(m.A);
        }

    }

    static class E
    {
        public static void Uus(this Mingi s) => Console.WriteLine(s.A);

        public static Mingi Ruut(this Mingi s) => new Mingi { A = s.A * s.A };

        public static string Tere(this string nimi) => $"Tere {nimi}!";

        public static string ToTitle(this string s)
        {
            if (s == "") return s;
            var ss = s.Split(' ');
            for (int i = 0; i < ss.Length; i++)
            {
                ss[i] = ss[i].Substring(0, 1).ToUpper() + ss[i].Substring(1).ToLower();
            }
            return string.Join(" ", ss);
        }

        public static IEnumerable<int> Paaris(this IEnumerable<int> m)
        {
            foreach(var x in m) if (x % 2 == 0) yield return x; 
        }
        public static IEnumerable<int> Paaritu(this IEnumerable<int> m)
        {
            foreach (var x in m) if (x % 2 == 1) yield return x;
        }
        public static IEnumerable<int> SuuremKui5(this IEnumerable<int> m)
        {
            foreach (var x in m) if (x > 5) yield return x;
        }

        public static IEnumerable<int> Milline(this IEnumerable<int> m, Func<int,bool> f) // where
        {
            foreach (var x in m) if (f(x)) yield return x;
        }

        public static bool KasPaaris(int x) 
        { 
            return x % 2 == 0; 
        }

        public static void Mida(this IEnumerable<int> m, Action<int> a) // .ForEach
        {
            foreach (var x in m) a(x);
        }

        public static string JoinToString<T>(this IEnumerable<T> m, string sep)
            => string.Join(sep, m);

        public static IEnumerable<TRes> TeeMidagi<TSource,TRes>(this IEnumerable<TSource> m , Func<TSource,TRes> f)
        {
            foreach (var x in m) yield return f(x);
        }


    }

}
