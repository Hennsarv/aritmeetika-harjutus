﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideAlgus
{
    class Program
    {
 
        static void Main(string[] args)
        {
            Inimene i = new Inimene { EesNimi = "Henn", PereNimi = "Sarv", Vanus = 65 };
            //i.SetVanus(65);
            //Trüki(i);

            //i.SetVanus(-100);
            //Trüki(i.GetVanus());

            //i.SetVanus(i.GetVanus() + 1);
            
            i.Vanus = 65;
            i.Vanus++;
            Trüki(i.Nimi);

        }



        // overload
        public static void Trüki(Inimene o)
        {
            Console.WriteLine(o);
        }

        public static void Trüki(int arv)
        {
            Console.WriteLine(arv);
        }

        public static void Trüki(int[] arvud)
        {
            Console.WriteLine(string.Join(",", arvud));
        }

        public static void Trüki(string s) => Console.WriteLine(s);


    }

  
}
